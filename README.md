# rails_docker

* 開発環境: http://localhost:3000/

## 開発環境ガイド

### 必要な道具
- git
- ruby
- gem
- [Docker](https://www.docker.com/get-started)
### ローカルでアプリケーションを起動させる

1. `docker-compose build`
2. `docker-compose up`

### アプリケーションを停止させる

`docker-compose down`

## インフラ編(docker tips)

### 使っていないコンテナをリムーブする

`docker rm $(docker ps -a -q)`

### イメージをリムーブする

`docker rmi $(docker images -q)`

### docker環境にコマンドを実行する場合
* docker環境でdb:migrateコマンドを実行する場合
`docker exec -ti {docker image name} rake db:migrate`
## 留意事項
- DL後、初起動までの流れ
- gitbucketから SourceTreeへクローンを実施
- SourceTree でローカルに develop ブランチを作成
- Docker を起動
  - Docker の初期設定
    - Windows のタスクバーからDocker（くじらのアイコン）のインジケーターを選択
    - メニューからSettings…を選択
    - Shared Deives を選択、Cドライブを設定
    - Advanced を選択、Memory：16384 に設定
    - Applyを押下
  - BIOSの変更
    - 使用しているPCに応じたBiosの設定方法を確認
    - BIOSで仮想化テクノロジーの使用を許可
    - (例) HPのPCは電源入り時にF10おしっぱでBIOSへ。Virtualization Technology(VTx)をenableへ変更
  - Windowsの機能の有効化
    - Windowsの機能を開く
    - Hyper-V 以下をすべてチェック ⇒ Docker is running となれば起動完了
Docker にてWebアプリを構築
  - コマンドプロンプトを起動し、プロジェクトのディレクトリに移動
  - 以下のコマンドでサービスをビルド
    `docker-compose build`
  - 以下のコマンドでサービス用のコンテナを構築し起動(※1)
    `docker-compose up`
  - 起動後以下のURLにアクセス
    http://localhost:3000/
